# ALIAS
function l
	ls -lhGF $argv
end

alias gst="git status"
alias gd="git diff"
alias gl="git pull"
alias gp="git push"
alias grv="git remote -v"
alias gcmsg="git commit -m"

# ENV VARS
set -x ANDROID_HOME ~/dev/android
set -x GOPATH ~/go
set -x PATH $PATH ~/.nimble/bin

# STARTUP
