import os
import osproc
import strformat
import environment
import common
import config

proc wipeDir(path: string) =
    for file in walkDirRec path:
        if file.existsDir(): wipeDir(file)
        else: file.removeFile()

proc newSession*(sessionName, dockerFile: string, useTor,
        encryptHome: bool): bool =

    try:
        if not isValidSessionName(sessionName):
            raise newException(Exception, "The session name must contain letters, numbers, dashes or dots, and start with a letter")

        if not existsFile(dockerFile):
            raise newException(OSError, "The dockerfile does not exist")

        let sessionPaths = getPathsForSession(sessionName)

        # Does it exist? (Docker cache)
        if existsDir(sessionPaths.dockerFolder):
            wipeDir(sessionPaths.dockerFolder)

        ensureSessionPaths(sessionPaths)

        # Read/update config
        var config: DX11SessionConfig
        if existsFile(sessionPaths.configFile):
            if not confirmAction("Warning: A session with this name already exists.\nDo you want to update it?", false):
                return false
            config = readSessionConfig(sessionName)

            # encryption status can't be changed
            if config.encryptHome != encryptHome:
                echo "Warning: The encryption setting can't be changed " & (
                        if config.encryptHome: "(enabled)" else: "(disabled)")

            # Update Tor settings only
            config.useTor = useTor
        else:
            config = DX11SessionConfig(useTor: useTor, encryptHome: encryptHome)

        # Software dependencies
        if not checkSetup(config.encryptHome): system.quit(1)

        # Store settings
        writeSessionConfig(sessionName, config)

        # Copy the dockerfile context to the local cache
        let dockerFilePath = splitFile(dockerFile)
        if dockerFilePath.dir == "":
            if expandFilename(".") != expandFilename(sessionPaths.dockerFolder):
                copyDir(".", sessionPaths.dockerFolder)
        else:
            if expandFilename(dockerFilePath.dir) != expandFilename(
                    sessionPaths.dockerFolder):
                copyDir(dockerFilePath.dir, sessionPaths.dockerFolder)

        # Ensure that Dockerfile exists on a canonical path
        if dockerFilePath.name != "Dockerfile" or dockerFilePath.ext != "":
            moveFile(joinPath(sessionPaths.dockerFolder,
                    dockerFilePath.name & dockerFilePath.ext),
            joinPath(sessionPaths.dockerFolder, "Dockerfile"))

        # Prepare the parameters

        let dockerImageName = fmt"dx11-{sessionName}"
        let cachedDockerFilePath = joinPath(sessionPaths.dockerFolder, "Dockerfile")
        let buildCommand = fmt"sudo docker build -t {dockerImageName} -f {cachedDockerFilePath} {sessionPaths.dockerFolder}"

        echo "Building the Docker image: " & dockerImageName

        let exitStatus = execCmd(buildCommand)
        if exitStatus != 0: return false
        return true

    except OSError:
        echo getCurrentExceptionMsg()
        return false
