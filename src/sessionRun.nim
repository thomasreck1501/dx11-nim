import os
import osproc
import strformat
import strutils
import common
import config
import environment

const DOCKER_USER = "user"
const CAPS_BASE = "--cap-add=SYS_MODULE --cap-add=SYS_RAWIO --cap-add=SYS_PACCT --cap-add=SYS_ADMIN --cap-add=SYS_NICE --cap-add=SYS_RESOURCE --cap-add=SYS_TIME --cap-add=AUDIT_CONTROL --cap-add=SYSLOG --cap-add=DAC_READ_SEARCH --cap-add=LINUX_IMMUTABLE --cap-add=IPC_LOCK --cap-add=IPC_OWNER --cap-add=SYS_PTRACE --cap-add=LEASE --cap-add=AUDIT_READ"
const USE_SYSTEMD = true

const GET_HOST_IP_CMD = "ip -4 addr show scope global dev docker0 | grep inet | awk '{print $2}' | cut -d / -f 1"
const HOST_DOMAIN = "host.local"

const CHECK_TOR_CMD = "sudo docker container ls | grep tor-router"
const LAUNCH_TOR_CMD = "sudo docker run -d --rm --name tor-router --cap-add NET_ADMIN --dns 127.0.0.1 flungo/tor-router"


## Check if TOR is running and start it otherwise
## Quits if it can't be started
proc startTor =
    let checkOk = execShellCmd(CHECK_TOR_CMD)
    if checkOk != 0:
        echo "Starting TOR"
        if execShellCmd(LAUNCH_TOR_CMD) != 0:
            raise newException(Exception, "Can't start the TOR network")

proc stopTor =
    echo "Stopping TOR"
    let status = execShellCmd("sudo docker container ls | grep flungo/tor-router | awk '{print $1}' | xargs sudo docker container stop")
    if status != 0: echo "FAILED"

## Mounts the encrypted folder using ecryptfs
## Quits if it fails
proc mountEncryptedHome(sessionPaths: DX11SessionPaths) =
    if execShellCmd("mount | grep \"^" & sessionPaths.homeSrcData &
            "\" > /dev/null") != 0:
        # mount
        let mountCommand = fmt"sudo -u {DOCKER_USER} ecryptfs-simple -a -c $HOME/.ecryptfs.config {sessionPaths.homeSrcData} {sessionPaths.homeEncryptedMount}"
        if execShellCmd(mountCommand) != 0:
            raise newException(Exception, "Unable to mount the encrypted home folder")

proc umountEncryptedHome(sessionPaths: DX11SessionPaths) =
    echo "Unmounting the encrypted folder"
    let status = execShellCmd(fmt"sudo -u {DOCKER_USER} ecryptfs-simple -u {sessionPaths.homeSrcData}")
    if status != 0: echo "FAILED"

proc runSession*(sessionName: string): bool =
    var sessionPaths: DX11SessionPaths
    var config: DX11SessionConfig
    try:
        if not isValidSessionName(sessionName):
            raise newException(Exception, "The session name is not valid")

        let pid = getCurrentProcessId()
        if execShellCmd(fmt"ps u | grep 'dx11\s*run\s*{sessionName}' | grep -v grep | grep -v {pid} > /dev/null") == 0:
            raise newException(Exception, "You are already runing " & sessionName)

        sessionPaths = getPathsForSession(sessionName)

        if not existsDir(sessionPaths.basePath) or not existsFile(
                sessionPaths.configFile):
            raise newException(OSError, "There is no such session: " & sessionName)

        config = readSessionConfig(sessionName)

        # Software dependencies
        if not checkSetup(config.encryptHome): system.quit(1)

    except Exception:
        echo getCurrentExceptionMsg()
        return false

    try:
        # Prepare the parameters
        let dockerImageName = fmt"dx11-{sessionName}"
        let systemdArgs = if USE_SYSTEMD: "--init=systemd --dbus=system" else: ""
        var homeOrigin = sessionPaths.homeSrcData
        var capsArgs = CAPS_BASE
        var torArgs = ""
        var hostBindArgs = ""

        # Keep track of the current VT
        var vtInfo = execCmdEx("fgconsole")
        var currentVtNumber: int
        var sessionVtNumber: int

        # if fgconsole fails it's because the process is running from
        # an X11 session => we need to change the VT
        var isOnX11 = vtInfo.exitCode != 0

        if isOnX11:
            # try os root
            vtInfo = execCmdEx("sudo fgconsole")
            if vtInfo.exitCode != 0:
                raise newException(Exception, "Can't open a new Virtual Terminal")
            currentVtNumber = vtInfo.output.replace("\n", "").parseInt()

            # Note the new VT
            vtInfo = execCmdEx("sudo fgconsole --next")
            if vtInfo.exitCode != 0:
                raise newException(Exception, "Can't open a new Virtual Terminal")
            sessionVtNumber = vtInfo.output.replace("\n", "").parseInt()
            if sessionVtNumber <= 1: sessionVtNumber = 8
        else:
            # otherwise, use the current VT for the session
            currentVtNumber = vtInfo.output.replace("\n", "").parseInt()
            sessionVtNumber = currentVtNumber

        # Check that the docker image exists
        if execShellCmd("sudo docker image ls | grep " & dockerImageName &
                " > /dev/null") != 0:
            raise newException(OSError, "The docker image does not exist. Try to build it first.")

        # Start the TOR container if needed
        if config.useTor:
            startTor()
            torArgs = "--net=container:tor-router"
            capsArgs = "--cap-add NET_ADMIN " & CAPS_BASE
        else:
            # Map the Host IP
            let ipData = execCmdEx(GET_HOST_IP_CMD)
            if ipData.exitCode == 0 and ipData.output is string and
                    ipData.output.len() > 0:
                let ipAddr = ipData.output.replace("\n", "").replace("\r", "")
                hostBindArgs = fmt"--add-host {HOST_DOMAIN}:{ipAddr} "

        # Mount the encrypted folder if needed
        if config.encryptHome:
            mountEncryptedHome(sessionPaths)

            homeOrigin = sessionPaths.homeEncryptedMount

        if isOnX11:
            discard execShellCmd(fmt"sudo chvt {sessionVtNumber}") # Go to the target VT

        # Run
        let runCommand = fmt"""sudo x11docker --hostipc {systemdArgs} --user={DOCKER_USER} --home={homeOrigin} -x --gpu -c --pulseaudio \
        --share {sessionPaths.shareFolder} --display {sessionVtNumber} --vt {sessionVtNumber} --cap-default \
        -- {capsArgs} {torArgs} {hostBindArgs} --volume-driver memfs -- {dockerImageName}"""

        echo "Using the Docker image: " & dockerImageName

        let exitStatus = execCmd(runCommand)
        if isOnX11:
            discard execShellCmd(fmt"sudo chvt {currentVtNumber}") # Back home
        if exitStatus != 0: return false

        # CLEANUP
        if config.useTor:
            stopTor()
        if config.encryptHome:
            umountEncryptedHome(sessionPaths)

        return true

    except Exception:
        echo getCurrentExceptionMsg()
        # CLEANUP
        if config.useTor:
            stopTor()
        if config.encryptHome:
            umountEncryptedHome(sessionPaths)
        return false

