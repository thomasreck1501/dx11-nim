import os
import osproc
import strformat
import environment
import common
import config

## Overwrites the current Docker image and builds it again
## ~/.local/share/dx11/<name>/docker/Dockerfile is expected to exist
proc rebuildSession*(sessionName: string): bool =

    try:
        if not isValidSessionName(sessionName):
            raise newException(Exception, "The session name must contain letters, numbers, dashes or dots, and start with a letter")

        let sessionPaths = getPathsForSession(sessionName)
        let cachedDockerFilePath = joinPath(sessionPaths.dockerFolder, "Dockerfile")
        let dockerImageName = fmt"dx11-{sessionName}"

        if not existsDir(sessionPaths.basePath) or not existsFile(
                sessionPaths.configFile):
            raise newException(OSError, "There is no such session: " & sessionName)

        # Does it exist? (Docker cache)
        if not existsDir(sessionPaths.dockerFolder) or not existsFile(
            cachedDockerFilePath):
            raise newException(Exception, "The Docker data does not exist")

        echo "This will overwrite the current Docker image and build it from scratch.\n"

        if confirmAction("Do you want to keep a backup of the current session image?", true):
            if execShellCmd(fmt"sudo docker tag {dockerImageName}:latest {dockerImageName}:bak") != 0:
                return false

        if not confirmAction("Do you want to rebuild the image?", true):
            return false

        ensureSessionPaths(sessionPaths)

        # Read/update config
        var config = readSessionConfig(sessionName)

        # Software dependencies
        if not checkSetup(config.encryptHome): system.quit(1)

        let buildCommand = fmt"sudo docker build --no-cache -t {dockerImageName} -f {cachedDockerFilePath} {sessionPaths.dockerFolder}"
        echo "Rebuilding the Docker image: " & dockerImageName

        let exitStatus = execCmd(buildCommand)
        if exitStatus != 0: return false
        return true

    except Exception:
        echo getCurrentExceptionMsg()
        return false
